package com.konradkevin.smsrecommander.di.components

interface DaggerComponentProvider {
    val component: ApplicationComponent
}