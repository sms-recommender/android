package com.konradkevin.smsrecommander.di.components

import android.content.Context
import com.konradkevin.smsrecommander.di.modules.DatabaseModule
import com.konradkevin.smsrecommander.di.modules.ViewModelsModule
import com.konradkevin.smsrecommander.ui.main.details.ContactsDetailsFragment
import com.konradkevin.smsrecommander.ui.main.list.ContactsListFragment
import com.konradkevin.smsrecommander.ui.main.MainActivity
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    DatabaseModule::class,
    ViewModelsModule::class
])
interface ApplicationComponent {

    fun inject(activity: MainActivity)
    fun inject(fragment: ContactsListFragment)
    fun inject(fragment: ContactsDetailsFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun applicationContext(applicationContext: Context): Builder
        fun databaseModule(databaseModule: DatabaseModule): Builder
        fun viewModelsModule(viewModelsModule: ViewModelsModule): Builder
        fun build(): ApplicationComponent
    }
}
