package com.konradkevin.smsrecommander.ui.main.list

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.konradkevin.smsrecommander.R
import com.konradkevin.smsrecommander.database.entities.ContactAndAllMessages
import com.konradkevin.smsrecommander.ui.base.BaseFragment
import com.konradkevin.smsrecommander.ui.main.MainViewModel
import com.konradkevin.smsrecommander.utils.injector
import kotlinx.android.synthetic.main.fragment_contacts_list.*

class ContactsListFragment: BaseFragment<MainViewModel>(R.layout.fragment_contacts_list, MainViewModel::class.java), ContactsListAdapter.OnItemClickListener {

    private val contactsListAdapter = ContactsListAdapter()

    override fun inject() {
        injector.inject(this)
    }

    override fun initUi() {
        contactsListAdapter.setOnItemClickListener(this)

        contacts_list_recycler_view.also {
            it.setHasFixedSize(true)
            it.layoutManager = LinearLayoutManager(context)
            it.adapter = contactsListAdapter
        }
    }

    override fun initObservers() {
        viewModel.contacts.observe(viewLifecycleOwner, Observer { contacts ->
            contacts.let { contactsListAdapter.submitList(contacts) }
        })
    }

    override fun onItemClick(contact: ContactAndAllMessages) {
        contact.contact.id?.let { viewModel.setContactId(it) }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ContactsListFragment()
    }
}