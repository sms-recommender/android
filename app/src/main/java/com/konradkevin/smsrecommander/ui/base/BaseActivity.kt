package com.konradkevin.smsrecommander.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.konradkevin.smsrecommander.di.ViewModelFactory
import javax.inject.Inject

abstract class BaseActivity<M: ViewModel> constructor(@LayoutRes val layoutRes: Int, private val vmClass: Class<M>): AppCompatActivity() {

    @Inject lateinit var viewModelFactory: ViewModelFactory
    protected lateinit var viewModel: M

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)
        inject()

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(vmClass)

        initUi()
        initObservers()
        initTransitions()
    }

    abstract fun inject()
    open fun initObservers() = Unit
    open fun initUi() = Unit
    open fun initTransitions() = Unit
}