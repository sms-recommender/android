package com.konradkevin.smsrecommander.ui.main.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.konradkevin.smsrecommander.R
import com.konradkevin.smsrecommander.database.entities.MessageEntity
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_contacts_details_item_me.*
import kotlinx.android.synthetic.main.fragment_contacts_details_item_other.*
import java.util.*

class MessagesListAdapter : ListAdapter<MessageEntity, MessagesListAdapter.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == ME) {
            ViewHolderMe(LayoutInflater.from(parent.context).inflate(R.layout.fragment_contacts_details_item_me, parent, false))
        } else {
            ViewHolderOther(LayoutInflater.from(parent.context).inflate(R.layout.fragment_contacts_details_item_other, parent, false))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let { message ->
            holder.itemView.tag = message
            holder.bind(message)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position).isItMe) {
            ME
        } else {
            OTHER
        }
    }

    companion object {
        private const val ME = 0
        private const val OTHER = 1
    }

    class ViewHolderOther(override val containerView: View) : ViewHolder(containerView), LayoutContainer {
        override fun bind(message: MessageEntity) {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = message.date
            val messageDateString = calendar.get(Calendar.HOUR_OF_DAY).toString() + ":" + calendar.get(Calendar.MINUTE).toString()

            contacts_details_item_message_other.text = message.content
            contacts_details_item_date_other.text = messageDateString
        }
    }

    class ViewHolderMe(override val containerView: View) : ViewHolder(containerView), LayoutContainer {
        override fun bind(message: MessageEntity) {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = message.date
            val messageDateString = calendar.get(Calendar.HOUR_OF_DAY).toString() + ":" + calendar.get(Calendar.MINUTE).toString()

            contacts_details_item_message_me.text = message.content
            contacts_details_item_date_me.text = messageDateString
        }
    }

    open class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        open fun bind(message: MessageEntity) {}
    }

    class DiffCallback : DiffUtil.ItemCallback<MessageEntity>() {
        override fun areItemsTheSame(oldItem: MessageEntity, newItem: MessageEntity) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: MessageEntity, newItem: MessageEntity) = oldItem == newItem
    }
}
