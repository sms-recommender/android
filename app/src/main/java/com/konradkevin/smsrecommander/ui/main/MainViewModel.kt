package com.konradkevin.smsrecommander.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.konradkevin.smsrecommander.database.dao.ContactDao
import com.konradkevin.smsrecommander.database.dao.ContactMessagesDao
import com.konradkevin.smsrecommander.database.dao.MessageDao
import com.konradkevin.smsrecommander.database.dao.RecommendationDao
import com.konradkevin.smsrecommander.database.entities.ContactAndAllMessages
import com.konradkevin.smsrecommander.database.entities.ContactEntity
import com.konradkevin.smsrecommander.database.entities.MessageEntity
import com.konradkevin.smsrecommander.database.entities.RecommendationEntity
import java.util.*
import javax.inject.Inject

class MainViewModel @Inject constructor(private val contactDao: ContactDao, contactMessagesDao: ContactMessagesDao, private val messageDao: MessageDao, private val recommendationDao: RecommendationDao) : ViewModel() {
    private val _contactId: MutableLiveData<Int?> = MutableLiveData(null)
    private val _userText: MutableLiveData<String> = MutableLiveData()
    private val _isItMe: MutableLiveData<Boolean> = MutableLiveData(true)

    val recommendations: LiveData<List<RecommendationEntity>> = Transformations.switchMap(_userText) { getRecommendationsFromUserEntry(it.toLowerCase(), 3) }
    val contactId: LiveData<Int?> = _contactId
    val contacts: LiveData<List<ContactAndAllMessages>> = contactMessagesDao.getAllContactsAndAllMessages()
    val contact: LiveData<ContactAndAllMessages> = Transformations.switchMap(_contactId) {
        it?.let { contactId -> contactMessagesDao.getContactAndAllMessages(contactId) }
    }

    fun setContactId(contactId: Int) {
        _contactId.value = contactId
    }

    fun setUserText(userText: String) {
        _userText.value = userText
    }

    fun addMessage(content: String) {
        if (content.isEmpty()) {
            return
        }

        _contactId.value?.let {contactId ->
            val message = MessageEntity(null, content, Date().time, contactId, _isItMe.value!!)
            messageDao.insert(message)
            _isItMe.value = !_isItMe.value!!
            updateContactMode()
        }
    }

    private fun updateContactMode() {
        contact.value?.let {
            var averageMode = 0.0

            it.messages.filter { message-> !message.isItMe } .forEach {message ->
                val words = message.content
                    .replace(".", "")
                    .replace("!", "")
                    .replace("?", "")
                    .replace(":", "")
                    .replace(";", "")
                    .split(" ")

                averageMode = words.fold(0.0) { sum, word ->
                    val recommendation = recommendationDao.getOne(word.toLowerCase())
                    if (recommendation != null && recommendation.mode != null) recommendation.mode + sum else 0.0 + sum
                } / words.size
            }

            it.contact.id?.let { contactId -> contactDao.updateMode(contactId, averageMode) }

        }
    }

    init {
        contactDao.insertAll(listOf(
            ContactEntity(0, "Me"),
            ContactEntity(1, "Yoann BRIANCOURT"),
            ContactEntity(2, "Thomas PAIN-SURGET"),
            ContactEntity(3, "Alexandre MÉNIELLE"),
            ContactEntity(4, "Hervé COIGNARD"),
            ContactEntity(5, "Caroline CHAUDET"),
            ContactEntity(6, "Benjamin MÉTAUT"),
            ContactEntity(7, "Fodé GUIRASSI")
        ))

        messageDao.insertAll(listOf(
            MessageEntity(0, "Coucou Kevin", Date().time, 1),
            MessageEntity(1, "Tu vas bien ?", Date().time, 1),
            MessageEntity(2, "Tu fais quoi ?", Date().time, 1),
            MessageEntity(3, "Regarde le meme que j'ai trouvé lol", Date().time, 1),

            MessageEntity(4, "Hellooo", Date().time, 5),
            MessageEntity(5, "Ca vaaaa ?", Date().time, 5),

            MessageEntity(6, "Yo", Date().time, 3),

            MessageEntity(14, "Apéroooooo", Date().time, 4),

            MessageEntity(7, "Wesh Kevin", Date().time, 7),
            MessageEntity(8, "On se fait un peu de Kotlin gros bâtard ?", Date().time, 7),

            MessageEntity(9, "Fdp", Date().time, 6),

            MessageEntity(10, "Ahah", Date().time, 1, true),
            MessageEntity(11, "Trop drôle !", Date().time, 1, true),
            MessageEntity(12, "Nique toi", Date().time, 7, true),
            MessageEntity(13, "Fdp toi même", Date().time, 6, true)
        ))
    }

    private fun getRecommendationsFromUserEntry(input: String, limit: Int): LiveData<List<RecommendationEntity>> {
        var userEntry = input

        if (userEntry.contains('.')) {
            userEntry = userEntry.split('.').last()
        }

        val words: List<String> = userEntry.split(' ').filter { s: String -> s.isNotBlank() }

        val word1 = if (words.isNotEmpty()) {
            if (words.size > 1) {
                words[words.size - 2]
            } else {
                words[0]
            }
        } else ""

        val word2 = if (words.isNotEmpty() && words.size > 1) {
            words[words.size - 1]
        } else ""

        if (userEntry.isNotEmpty() && userEntry.last() != ' ') {
            return recommendationDao.get1GramsWords("${words.last()}%", limit)
        }

        if (words.size > 1) {
            return recommendationDao.get3GramsWords(word1, word2, limit)
        }

        if (words.size == 1) {
            return recommendationDao.get2GramsWords(word1, limit)
        }

        return if (input.isEmpty()) {
            recommendationDao.getStarterWords(limit)
        } else {
            recommendationDao.getMostFrequentWords(limit)
        }
    }
}