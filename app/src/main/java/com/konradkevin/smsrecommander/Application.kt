package com.konradkevin.smsrecommander

import android.app.Application
import com.konradkevin.smsrecommander.di.components.ApplicationComponent
import com.konradkevin.smsrecommander.di.components.DaggerApplicationComponent
import com.konradkevin.smsrecommander.di.components.DaggerComponentProvider
import com.konradkevin.smsrecommander.di.modules.DatabaseModule
import com.konradkevin.smsrecommander.di.modules.ViewModelsModule

class Application : Application(), DaggerComponentProvider {
    override val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationContext(applicationContext)
            .databaseModule(DatabaseModule)
            .viewModelsModule(ViewModelsModule)
            .build()
    }
}