package com.konradkevin.smsrecommander.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "recommendations")
data class RecommendationEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "mot1")
    val word1: String,

    @ColumnInfo(name = "mot2")
    val word2: String?,

    @ColumnInfo(name = "mot3")
    val word3: String?,

    @ColumnInfo(name = "counter")
    val counter: Int?,

    @ColumnInfo(name = "is_idiomatic")
    val isIdiomatic: Boolean?,

    @ColumnInfo(name = "mode")
    val mode: Double?,

    @ColumnInfo(name = "is_starter")
    val isStarter: Boolean
)