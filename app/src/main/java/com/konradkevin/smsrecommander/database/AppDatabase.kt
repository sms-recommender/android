package com.konradkevin.smsrecommander.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.konradkevin.smsrecommander.database.dao.ContactDao
import com.konradkevin.smsrecommander.database.dao.ContactMessagesDao
import com.konradkevin.smsrecommander.database.dao.MessageDao
import com.konradkevin.smsrecommander.database.dao.RecommendationDao
import com.konradkevin.smsrecommander.database.entities.ContactEntity
import com.konradkevin.smsrecommander.database.entities.MessageEntity
import com.konradkevin.smsrecommander.database.entities.RecommendationEntity

@Database(
    version = 7,
    entities = [
        ContactEntity::class,
        MessageEntity::class,
        RecommendationEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun contactDao(): ContactDao
    abstract fun contactMessagesDao(): ContactMessagesDao
    abstract fun messageDao(): MessageDao
    abstract fun recommendationsDao(): RecommendationDao
}