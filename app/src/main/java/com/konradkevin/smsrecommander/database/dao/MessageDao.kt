package com.konradkevin.smsrecommander.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.konradkevin.smsrecommander.database.entities.MessageEntity

@Dao
interface MessageDao: BaseDao<MessageEntity> {
    @Query("SELECT * FROM messages ORDER BY id DESC")
    fun getAll(): LiveData<List<MessageEntity>>
}